
# Приложение с прогнозом погоды

Небольшое тестовое приложение, в котором отображается прогноз погоды на 5 ближайших дней в выбранном городе


## Функционал

- Температура
- Максимальная скорость ветра, м/с
- Влажность воздуха
- Краткое описание погоды и соответствующая иконка


## Screenshots

<img src="https://github.com/AntonTuritsyn/Weather/blob/master/Screenshot_20230823_191059.png" width=30% height=30%>

